# LearningKotlin

This repository will have the practices and exercises that I will doing while studying Kotlin ✨

### Kotlin version 1.5.31

### First module (from official documentation):

#### Basics:
- Basic syntax              ✅
- Idioms                    ✅                
- Coding Conventions         ✅

### second module (from official documentation):

#### concepts:

- Types                      ✅
- Control flow               ⬜
   

