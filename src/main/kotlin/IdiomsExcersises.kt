fun main(){

    obj.doSomething()
    println()
    obj.sleep()
}

abstract class MyAbstractClass{
    abstract fun doSomething()
    abstract fun sleep()
}

//instantiate an abstract class
val obj = object : MyAbstractClass() {
    override fun doSomething() {
        print("doing something")
    }

    override fun sleep() {
        print("sleeping")
    }
}

