fun main() {

        //Standar output
        print("Function that allow print a line")
        println("function that allow to print a line with a jump")


        print(sum(4,7))
        println()
        print(text("isaias"))
        println()
        println(myRectangle.perimeter)

        println(word1)
        println(word2)

        println(maxOf(6,8))
        println(maxOfTwo(9,4))

        printSimpleNumbersSequence()
        forEach()

    val myArray = listOf("apple", "banana", "kiwifruit")
    var myIndex = 0

    while (myIndex < myArray.size) {
        println("item at $myIndex is ${myArray[myIndex]}")
        myIndex++
    }

    println()
    println(describe("Hello"))



    //checking if number is in range
    val r = 10
    var q = 9

    if(r in 1..q+1){
        println("fits is in range")
    }

    // checking if number is out of range
    val coll = listOf("a","b","c")

    if(-1 !in 1..coll.lastIndex){
        println("-1 is out of range")
    }

    if(coll.size !in coll.indices)
        println("list size is out of a list valid index too")

    //progressions

    for(x in 1..20 step 2){ //step make us available to specify a progression inside de loop declaration. is better than: x=x+2 inside the loop's body
        print("$x, ")
    }

    for (x in 12 downTo 0 step 3){
        print("$x, ")
    }

    // COLLECTIONS

    // Iterate a collection

    val myCollection = listOf("Banana", "Avocado", "Apple", "Kiwifruit", "Orange")
    for(item in myCollection){
        print("$item, ")
        // Check if an element is in a collection using "in" operator
    }

    // Check if an element is in a collection
    when{
        myCollection.contains("Banana") -> println("Banana is on the collection")
    }

    val fruitItems = setOf("apple", "banana", "kiwifruit")
    when {
        "orange" in fruitItems -> println("juicy")
        "apple" in fruitItems -> println("apple is fine too")
    }

    sumOfNull("7", "6")
    sumOfNull("7", "6o")

    } //**********************end of main function

    // function that return an integer value
    fun sum(i: Int, b: Int): Int{
        return i + b
    }


    //Function that return an string value
    fun text(name: String): String{

        //we don't need to concatenate values, we can just use template as below
        return "hello $name"
    }

    //Function that don't return nothing (use of Unit)
    fun nothing(): Unit{
        print("This function don't return anything")
    }

/*
*
*       VARIABLES VAL AND VAR
*
* */

    //Read only variables: Val
    val a: Int = 1  // immediate assignment
    val b = 2   // `Int` type is inferred


    //reassigned variables: var

    var s = 5 // `Int` type is inferred

val PI = 3.14
var x = 0

fun incrementX() {
    x += 1
}

/*
*
*       CLASSES
*
* */

class Shape

// we can declare the properties of a class in the class declaration:
class Rectangle(var height: Double, var width: Double){
    var perimeter = (height+width) * 2
}

//when we create an instance for this class, el constructor with these parameters is called by default
var myRectangle = Rectangle(5.0, 2.0)


// to make a class inheritable, we need to mark it using "open"
open class Shape1

class Rectangle1(var height: Double, var length: Double): Shape1() {
    var perimeter = (height + length) * 2
}

/*
*
*       STRINGS TEMPLATES
*
* */

//Is a more elegant way of concatenate or include logic in a simple string expression

var v1 = 1

val word1 = "this is a simple use of Strings template, the value of variable b1 is $v1"

var v2 =5
//arbitrary expression in template:
val word2 = "${word1.replace("is", "was")}, but now is $v2"


/*
*
*       CONDITIONAL EXPRESSIONS
*
* */

fun maxOf(a: Int, b: Int): Int{
    if(a > b) {
        return a
    }
    else{
        return b
    }
}

// in kotlin we can use conditional has an expression:
fun maxOfTwo(a: Int, b: Int) = if(a > b) a else b

/*
*
*       FOR LOOP
*
* */

fun printSimpleNumbersSequence(): Unit{
    for(i in 1..10)
    {
        println(i)
    }

    for(a in 20 downTo 5){
        println(a)
    }

    //print numbers from 50 to 5 jumping down 5 numbers
    for (x in 50 downTo 5 step 5){
        print("$x ")
    }
}



// in collections:
fun forEach(){
    val items = listOf("blue","white","black","green")

    for (item in items){
        println(item)
    }

    //getting the index
    for(index in items.indices){
        println("item at $index is ${items[index]}")
    }
}

/*
*
*       WHILE LOOP: see lines from 23 to 30
*
* */

/*
*
*       WHEN EXPRESSION: is a control flow structure which use is similar to a switch in java
*
* */

//an "any" parameter is like object in java, can be use to store any type of value
fun describe(obj: Any): String =
    when (obj){
        1 ->    "one"
        "Hello" -> "Greeting"
        String -> "An String"
        !is String -> "Is not an String"
        else -> "unknown"
    }

/*
*
*       RANGES : check inside main function
*
* */

/*
*
*       COLLECTIONS : check inside main function
*
* */

/*
*
*       NULLABLE VALUES AND NULL CHECKS
*
* */

//are parameters, arguments and objects that que be null and are reference using "?"

//  examples

fun parseInt(arg: String): Int?{
    return arg.toIntOrNull()
}

fun sumOfNull(arg1: String, arg2: String){

    val x= parseInt(arg1)
    var y= parseInt(arg2)

    if(x != null && y != null){
        println("the sum of $x + $y = ${x+y}")
    }else{
        println("the value $x or $y is not a number")
    }
}

/*
*
*       TYPE CHECKS AND AUTOMATIC CASTS
*
* */

//The is operator checks if an expression is an instance of a type. If an immutable local variable or property is checked for a specific type, there's no need to cast it explicitly


fun getStringLength(obj: Any): Int?{

    if(obj is String){
        //the "obj" parameter is casted by the "is" operator and we can use now has the new type
        return obj.length
    }
    return null
}

// using negative form for operator "is"
fun getSimpleStringLength(obj: Any): Int?{
    if(obj !is String) return null

    return obj.length
}


//  https://kotlinlang.org/docs/basic-syntax.html  FINISHED !!