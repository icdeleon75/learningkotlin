fun main(){

    //build-it types

    //Byte: since -127 to 127
    //short since -32768 to 32767
    //int:  since 	-2,147,483,648 (-2 31) to	2,147,483,647 (2 31- 1)
    //long: -9,223,372,036,854,775,808 (-2 63)	9,223,372,036,854,775,807 (2 63- 1)

    val one = 1 // int
    val threeBillion = 3000000000 // long
    val oneLong = 1L // we use suffix "L" to specify that a variable is long if its size is smaller than Int.
    val oneByte: Byte = 1

    //double and float values

    //if we just specify decimal values by default it will be interpreted has a Double value:

    val i = 1.0 // is a double
    val d = 1.2345677 // is a double
    val pi = 3.14 // double

    //we must to specify that a number is float, using suffix "f" or "F"

    val iFloat = 1.0f // is a float
    val dFloat = 1.2345677 // is a float

    //There are not implicit number conversion in kotlin

    fun printDouble(d: Double) {println(d)}

    val q = 1
    val w = 1.0
    val e = 1.0f

    printDouble(w)
//    printDouble(q) // Error: Type mismatch
//    printDouble(e) // Error: Type mismatch

    // literal Constants
    // allow us to use underscore to make a value more easier for read
     val oneMillion = 1_000_000
     var creditCardNumber = 1234_5678_9999_1111L

    print(oneMillion+creditCardNumber)


    // Explicit conversion

    // In kotlin a smaller type is not implicit convert to the bigger type, let's check

    //val a: Int? = 1
   // var b: Long? = a  is an error
   // println(a == b) // return false

    // an explicit conversion example

    val byte: Byte =1;
    val int1: Int = byte.toInt()

    // Boleans

    val myTrue: Boolean = true
    val myFalse: Boolean = false
    val myNull: Boolean? = null

    println(myFalse || myTrue)  // true
    println(myTrue && myFalse)  // false
    println(!myTrue)            // false


    // Characters

    val aChar: Char = 'a'
    println(aChar)
    println('\n') //prints an extra newLine character
    println('\uFF00')

    // Strings

    val abs = "abs"

    // the string variables are a group of characters, so we can access to it using a loop
    for (c in abs){
        println(c)
    }

    // Once you initialize a string, you can't change its value or assign a new value to it. All operations that transform strings return their results in a new String object

    val str = "hello"
    println(str.uppercase()) // create a new String value
    println(str)             // the original

    //concatenate strings

    val s = "abc" + 1
    println(s + "def")

    // Raw strings : can contain newlines and other characters, are creating use three double quotes (""")

    val raw ="""
        for(c in "foo")
        {
           println(c)
        }
    """

    println(raw)

    val text = """
    |Tell me and I forget.
    |Teach me and I remember.
    |Involve me and I learn.
    |(Benjamin Franklin)
    """.trimMargin()

    println(text)


    // Strings templates

    val t1 = 1
    println("the value of t1 = $t1")

    // we can also use expressions
    val t = "qwerty"
    println("$t.length is ${t.length}")


    // Arrays

    // In kotlin the arrays are represented by the Array class

    //creating a simple array of string
    val stringArray = arrayOf("one", "two", "three", "four")
    for (index in stringArray.indices){
        println("the value at index $index is ${stringArray[index]}")
    }

    // a simple array of int
    val myIntArray = arrayOf(4,5,6,83,5)
    var sum:Int =0
    for (c in 0 until myIntArray.size){
        sum += myIntArray[c]
    }
    println("the sum of the elements on myIntArray is equal to $sum")

    

}